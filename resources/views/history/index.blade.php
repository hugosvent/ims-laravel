@extends('layouts.app')

@section('content')
<div class="container">
    <!--Start Item List-->
    <div class="panel panel-default">
        <div class="panel-heading padding-title">
            <div class="row">
                <div class="col-xs-7">
                    <div style="margin-top: -5px;">
                        <h3><span class="glyphicon glyphicon-list"></span> Riwayat Barang {{ $item->name }} </h3>
                    </div>
                </div>
                <div class="col-xs-5">
                    <form class="navbar-form hidden-xs navbar-right form-inline" role="form" id="search-form-hidden-xs" name="search-form" action="{{ url('/item') }}">
                        <div class="form-group" style="margin-top:5px;">
                            <input type="text" class="form-control" placeholder="cari barang" id="searchItem" name="search" style="width:300px;" value="{{ old('search') }}"/>
                            <button type="submit" class="btn"><span class="glyphicon glyphicon-search"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel-body" style="height: 100%; overflow: auto;">
            <!--Start Item Table-->
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                     @foreach ($histories as $history)
                        <tr>
                            <td>{{ $history->created_at }}</td>
                            <td>{{ $history->action }} </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <!--End Item Table-->
            {!! $histories->links() !!}
        </div>
    </div>
    <!--End Item List-->
</div>



@endsection