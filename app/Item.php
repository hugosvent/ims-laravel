<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public static function get($search = null) {
        if ($search) {
            return self::where('code', 'like', "%$search%")->orwhere('name', 'like', "%$search%")->orderBy('code','asc')->paginate(15);
        } else {
            return self::orderBy('code','asc')->paginate(15);
        }
    }

    public function getCreateduserAttribute() {
        return \App\User::where('id',$this->created_by)->first();
    }

    public function getUpdateduserAttribute() {
        return \App\User::where('id',$this->updated_by)->first();
    }

    public function histories()
    {
        return $this->hasMany('App\History')->orderBy('created_at','desc');
    }
}
