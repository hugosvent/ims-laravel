# Inventory Management System #
Tugas Interaksi Manusia dan Komputer Mobile dan Web Sore

## Getting Started
Buka link ini untuk melihat [demo](http://imk.buburbulan.xyz) aplikasi.

## Built with ##
* [Laravel](http://laravel.com)
* [MySQL](http://mysql.com)
* [Nginx](http://nginx.com)
* [Docker](http:://docker.com)

## Author ##
* **Jackyson** - *131110674*
* **Javentira Lienata** - *131110950*
* **Muhammad Rizky Hasibuan** - *131112420*

## Features ##
* Autentikasi Pengguna
* Cek Stok Barang
* Masukkan Stok Barang Baru
* Hapus Stok Barang
* Edit Stok Barang
* Lihat Riwayat Perubahan

## Usage ##
Saat pertama kali membuka aplikasi, pengguna akan diarahkan terlebih dahulu ke halaman *login* untuk masuk ke akun yang sudah pernah didaftarkan sebelumnya. Apabila pengguna belum memiliki akun, maka pengguna harus mendaftarkan akun baru terlebih dahulu di halaman *register* yang akan menampilkan form untuk dilengkapi oleh calon pengguna.

Apabila pengguna telah berhasil mendaftarkan akun baru, aplikasi akan mengarahkan pengguna ke tampilan *dashboard* yang akan menampilkan daftar stok barang yang dimasukkan pengguna. Saat pertama kali pengguna melihat halaman ini, maka pengguna akan melihat sebuah tabel kosong.

Pada bagian atas, pengguna dapat melihat tombol *tambah barang* untuk memasukkan stok barang baru. Apabila pengguna telah memiliki barang, maka akan disediakan menu untuk melakukan *pengeditan* dan *pengapusan* data barang, serta melihat inforasi riwayat perubahan barang.

## Install ##

* Untuk **windows** download dan install [xampp](https://www.apachefriends.org/download.html). Untuk **ubuntu** pastikan anda telah terinstall [nginx](https://nginx.com) dan [phpmyadmin](https://phpmyadmin.net)

* Download dan install [composer](https://getcomposer.org)

* Download dan install [nodejs](https://nodejs.org)

* Setelah *nodejs* terpasang, install *gulp* dengan membuka terminal atau command prompt, lalu ketik `npm install -g gulp`

* Setelah itu masuk ke dalam folder anda menyimpan project ini

* Buka terminal atau command prompt didalam folder dan ketik 'composer install'

* Setelah itu ubah file `.env.example` menjadi `.env` dan masukkan konfigurasi database anda 
`DB_HOST=localhost`
`DB_CONNECTION=mysql`
`DB_DATABASE=nama_database_di_phpmyadmin` `DB_USERNAME=nama_pengguna_untuk_login_database` `DB_PASSWORD=kata_sandi_untuk_masuk_database'`

* Setelah itu `php artisan migrate`

* Setelah itu `php artisan key:generate`

* Setelah itu `npm install`

* Setelah itu `gulp`

* Setelah semuanya berhasil, ketik `php artisan serve` dan buka `localhost:8000` di browser untuk mulai menggunakan aplikasi